package hotel.exception;

public class UnknownOptionException extends HotelServiceException {

    public UnknownOptionException() {
        super("unknown option");
    }

}
