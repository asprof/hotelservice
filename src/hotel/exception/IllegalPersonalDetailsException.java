package hotel.exception;

public class IllegalPersonalDetailsException extends HotelServiceException {
    public IllegalPersonalDetailsException(String string) {
        super(string + " - invalid personal details!");
    }
}
