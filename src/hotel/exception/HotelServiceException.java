package hotel.exception;

public class HotelServiceException extends RuntimeException {

    public HotelServiceException(String message) {
        super(message);
    }
}
