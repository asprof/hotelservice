package hotel.exception;

import java.util.NoSuchElementException;

public class NoRoomsFoundException extends HotelServiceException {
    public NoRoomsFoundException() {
        super("no rooms found");
    }
}
