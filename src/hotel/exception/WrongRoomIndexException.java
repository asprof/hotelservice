package hotel.exception;

public class WrongRoomIndexException extends HotelServiceException {
    public WrongRoomIndexException() {
        super("wrong room number");
    }
}
