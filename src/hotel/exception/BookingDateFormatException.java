package hotel.exception;

public class BookingDateFormatException extends HotelServiceException {
    public BookingDateFormatException(String message) {
        super("unexpeted format \""+ message + "\" , use yyyy-MM-dd");
    }
}
