package hotel.service;

import hotel.exception.WrongRoomIndexException;
import hotel.model.Guest;
import hotel.model.Hotel;
import hotel.model.Room;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class UserService {

    private Hotel hotel = new Hotel();

    public UserService() {
    }

    public List<Room> getAllRoom() {
        return hotel.getRoomList();
    }

    public Room getRoom(int index) {
        return hotel.getRoomByNumber(index).get();
    }

    private Optional<Room> getOptRoom(int index) {
        return hotel.getRoomByNumber(index);
    }

    public List<Room> getAvailableAndCleanedRoom() {
        return getAllRoom().stream()
                .filter(Room::getAvailable)
                .filter(Room::getCleaned)
                .collect(Collectors.toList());
    }

    public List<Room> getRentedRoom() {
        return getAllRoom().stream()
                .filter(n -> !n.getAvailable())
                .collect(Collectors.toList());
    }

    public List<Room> getNotCleanedRoom() {
        return getAllRoom().stream()
                .filter(n -> !n.getCleaned())
                .collect(Collectors.toList());
    }

    public boolean bookRoom(int roomNumber, LocalDate checkIn, LocalDate checkOut, List<Guest> guestList) {
        Optional<Room> room = getOptRoom(roomNumber);
        room.ifPresent(r -> {
            r.setCheckInDate(checkIn);
            r.setCheckOutDate(checkOut);
            r.setGuests(guestList);
        });
        return room.map(Room::bookRoom).orElse(false);
    }

    public boolean releaseRoom(int roomNumber) {
        Optional<Room> room = getOptRoom(roomNumber);
        return room.map(Room::releaseRoom).orElse(false);
    }

    public boolean cleanRoom(int roomNumber) {
        Optional<Room> room = getOptRoom(roomNumber);
        return room.map(Room::cleanRoom).orElse(false);
    }

    public static Room findRoom(int index, List<Room> roomList) {
        if (Objects.isNull(roomList) || index >= roomList.size() || index < 0) {
            throw new WrongRoomIndexException();
        }
        return roomList.get(index);
    }

    public Guest createGuest(String name, String surname, LocalDate birth) {
        return new Guest.GuestBuilder()
                .setName(name)
                .setSurname(surname)
                .setBorn(birth)
                .build();
    }
}
