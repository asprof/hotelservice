package hotel.model;

import java.time.LocalDate;
import java.util.Objects;

public class Guest {

    private String name;
    private String surname;
    private LocalDate born;

    private Guest() {}

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public LocalDate getBorn(){
        return born;
    }

    public int getAge() {
        LocalDate now = LocalDate.now();
        return now.minusYears(born.getYear()).getYear();
    }

    public GuestBuilder buildGuest(){
        return new GuestBuilder();
    }

    public static class GuestBuilder {
        private Guest guest;

        public GuestBuilder() {
            guest = new Guest();
        }

        public GuestBuilder setName(final String name) {
            guest.name = name;
            return this;
        }

        public GuestBuilder setSurname(final String surname) {
            guest.surname = surname;
            return this;
        }

        public GuestBuilder setBorn(final LocalDate born) {
            guest.born = born;
            return this;
        }

        public Guest build() {
            return guest;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, born);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Guest guest = (Guest) obj;
        return name == guest.name &&
                surname == guest.surname &&
                born == guest.born;
    }

    @Override
    public String toString() {
        StringBuilder personalDetails = new StringBuilder();
        String space = " ";
        personalDetails.append(name)
                .append(space)
                .append(surname)
                .append(" born: ")
                .append(born);
        return personalDetails.toString();
    }
}