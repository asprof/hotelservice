package hotel.model;

public enum PersonalDetails {
    NAME, SURNAME, DATE_OF_BIRTH
}
