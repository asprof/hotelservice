package hotel.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Hotel {

    private List<Room> roomList = new ArrayList<>();

    public Hotel() {
        buildDefaultRooms();
    }

    private void buildDefaultRooms() {
        roomList.add(createSingleRoom(1, 1));
        roomList.add(createSingleRoom(2, 1));
        roomList.add(createSingleRoom(3, 1));
        roomList.add(createSingleRoom(4, 1));
        roomList.add(createSingleRoom(5, 2));
//        roomList.add(createSingleRoom(6,2));
//        roomList.add(createSingleRoom(7,2));
//        roomList.add(createSingleRoom(8,2));
//        roomList.add(createSingleRoom(9,2));
//        roomList.add(createSingleRoom(10,2));
//        roomList.add(createSingleRoom(11,3));
//        roomList.add(createSingleRoom(12,3));
//        roomList.add(createSingleRoom(13,4));
//        roomList.add(createSingleRoom(14,4));
//        roomList.add(createSingleRoom(15,4));
//        roomList.add(createSingleRoom(16,4));
//        roomList.add(createSingleRoom(17,8));
//        roomList.add(createSingleRoom(18,8));
//        roomList.add(createSingleRoom(19,8));
//        roomList.add(createSingleRoom(20,8));
    }

    private Room createSingleRoom(int number, int howManyPeople) {
        return Room.buildCustomRoom()
                .withNumber(number)
                .withQuantityPeople(howManyPeople)
                .withBathroom(true)
                .withAvailable(true)
                .withCleaned(true)
                .withCheckInDate(LocalDate.MIN)
                .withCheckOutDate(LocalDate.MAX)
                .withGuestsList(new ArrayList<>())
                .build();
    }

    public List<Room> getRoomList() {
        return new ArrayList<>(roomList);
    }

    public Optional<Room> getRoomByNumber(int roomNumber) {
        return roomList.stream().filter(n -> n.getNumber() == roomNumber).findFirst();
    }

}
