package hotel.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Room {

    private int number;
    private int howManyPeople;
    private boolean bathroom;
    private boolean available;
    private boolean cleaned;
    private List<Guest> guests;
    private LocalDate checkInDate;
    private LocalDate checkOutDate;

    private Room() {}

    public int getNumber() {
        return number;
    }

    public int getHowManyPeople() {
        return howManyPeople;
    }

    public boolean getBathroom() {
        return bathroom;
    }

    public boolean getAvailable() {
        return available;
    }

    public boolean getCleaned() {
        return cleaned;
    }

    public LocalDate getCheckInDate() {
        return checkInDate;
    }

    public LocalDate getCheckOutDate() {
        return checkOutDate;
    }

    public List<Guest> getListGuests() {
        return new ArrayList<>(guests);
    }

    public void setGuests(List<Guest> guests) {
        this.guests = guests;
    }

    public void setCheckInDate(LocalDate checkInDate) {
        this.checkInDate = checkInDate;
    }

    public void setCheckOutDate(LocalDate checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public boolean bookRoom() {
        boolean success = false;
        if (getAvailable() && getCleaned()) {
            available = false;
            success = true;
        }
        return success;
    }

    public boolean releaseRoom() {
        boolean success = false;
        if (!getAvailable()) {
            available = true;
            cleaned = false;
            success = true;
        }
        return success;
    }

    public boolean cleanRoom() {
        boolean success = false;
        if (!cleaned) {
            cleaned = true;
            guests.clear();
            success = true;
        }
        return success;
    }

    public static RoomBuilder buildCustomRoom() {
       return new RoomBuilder();
    }

    public static RoomBuilder buildCustumRoom(){
        return new RoomBuilder();
    }

    public static class RoomBuilder {
        private Room room;

        private RoomBuilder() {
            room = new Room();
        }

        public RoomBuilder withNumber(final int number) {
            room.number = number;
            return this;
        }

        public RoomBuilder withQuantityPeople(final int howManyPeople) {
            room.howManyPeople = howManyPeople;
            return this;
        }

        public RoomBuilder withBathroom(final boolean bathroom) {
            room.bathroom = bathroom;
            return this;
        }

        public RoomBuilder withAvailable(boolean available) {
            room.available = available;
            return this;
        }

        public RoomBuilder withCleaned(boolean cleaned) {
            room.cleaned = cleaned;
            return this;
        }

        public RoomBuilder withGuestsList(final List<Guest> guestList) {
            room.guests = guestList;
            return this;
        }

        public RoomBuilder withCheckInDate(final LocalDate checkInDate) {
            room.checkInDate = checkInDate;
            return this;
        }

        public RoomBuilder withCheckOutDate(final LocalDate checkOutDate) {
            room.checkOutDate = checkOutDate;
            return this;
        }

        public Room build() {
            return room;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Room room = (Room) o;
        return number == room.number &&
                howManyPeople == room.howManyPeople &&
                bathroom == room.bathroom &&
                available == room.available &&
                checkInDate == room.checkInDate &&
                checkOutDate == room.checkOutDate &&
                guests == room.guests;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, howManyPeople, bathroom,
                available, checkInDate, checkOutDate, guests);
    }

    @Override
    public String toString() {
        StringBuilder room = new StringBuilder();
        room.append("Room{")
                .append("number=").append(number)
                .append(", number of people=").append(howManyPeople)
                .append(", bathroom=").append(bathroom)
                .append(", available=").append(available)
                .append(", cleaned=").append(cleaned)
                .append(", check in=").append(checkInDate)
                .append(", check out=").append(checkOutDate)
                .append(", ").append(guests)
                .append("}");
        return room.toString();
    }
}
