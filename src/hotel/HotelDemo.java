package hotel;

import hotel.front.Controller;
import hotel.service.UserService;

public class HotelDemo {

    public static void main(String[] args) {
        UserService userService = new UserService();
        Controller controller = new Controller(userService);
        controller.start();

    }

}
