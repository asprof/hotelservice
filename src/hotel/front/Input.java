package hotel.front;

import hotel.exception.BookingDateFormatException;
import hotel.exception.IllegalPersonalDetailsException;
import hotel.exception.UnknownOptionException;
import hotel.model.Room;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Input {

    private Scanner scanner = new Scanner(System.in);

    protected String nextLine() {
        boolean wenttocatch = false;
        scanner = new Scanner(System.in);
        String value = null;
        do {
            if (scanner.hasNextLine()) {
                value = scanner.nextLine();
                wenttocatch = true;
            } else {
                scanner.next();
                System.err.println("Enter a valid String value!");
            }
        } while (!wenttocatch);
        return value;
    }

    protected int nextInt() {
        boolean wenttocatch = false;
        scanner = new Scanner(System.in);
        int value = 0;
        do {
            if (scanner.hasNextInt()) {
                value = scanner.nextInt();
                wenttocatch = true;
            } else {
                scanner.nextLine();
                System.err.println("Enter a valid Integer value!");
            }
        } while (!wenttocatch);
        return value;
    }

    protected int mainOption() {
        int option = nextInt();
        validate(option);
        return option;
    }

    private void validate(int input) {
        if (!isValidOption(input)) {
            throw new UnknownOptionException();
        }
    }

    private boolean isValidOption(int input) {
        return input >= 0 && input < 7;
    }

    protected void close() {
        scanner.close();
    }

    protected int getValidRoom(List<Room> roomList) {
        return checkRoom(roomList);
    }

    protected String getValidName() {
        return checkData(nextLine());
    }

    protected String getValidSurname() {
        return checkData(nextLine());
    }

    protected LocalDate getValidDate() {
        return checkDate(nextLine());
    }

    private int checkRoom(List<Room> roomList) {
        int input = nextInt();
        if (input > roomList.size() || input < 0) {
            throw new UnknownOptionException();
        }
        return input;
    }

    private String checkData(String personalData) {
        if (!check(personalData)) {
            throw new IllegalPersonalDetailsException(personalData);
        }
        return personalData.toUpperCase();
    }

    private LocalDate checkDate(String born) {
        LocalDate date = parseStringToDate(born);
        if (!validateDate(date)) {
            throw new IllegalPersonalDetailsException(born);
        }
        return date;
    }

    private boolean check(String personalData) {
        String regex = "^[A-Za-z][a-z]{1,100}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(personalData);
        return matcher.matches();
    }

    private LocalDate parseStringToDate(String born) {
        LocalDate date = null;
        try {
            date = LocalDate.parse(born);
        } catch (DateTimeParseException e) {
            System.err.println(e.getMessage());
        }
        return date;
    }

    private boolean validateDate(LocalDate born) {
        boolean success = false;
        if (isAfter(born) && isEarlier(born)) {
            success = true;
        }
        return success;
    }

    private boolean isAfter(LocalDate localDate) {
        return LocalDate.now().isAfter(localDate);
    }

    private boolean isEarlier(LocalDate born) {
        final int maxPersonLife = 130;
        return born.isAfter(LocalDate.now().minusYears(maxPersonLife));
    }

    public LocalDate getValidAndParseDate() {
        LocalDate date = parseDate();
        if (!isBefore(date)) {
            throw new IllegalPersonalDetailsException(date.toString());
        }
        return date;
    }

    private LocalDate parseDate() {
        String checkIn = nextLine();
        LocalDate parsed;
        try {
            parsed = LocalDate.parse(checkIn);
        } catch (DateTimeParseException e) {
            throw new BookingDateFormatException(e.getParsedString());
        }
        return parsed;
    }

    private boolean isBefore(LocalDate date) {
        return LocalDate.now().isBefore(date);
    }

    public LocalDate wheterCheckInDateOlder(LocalDate checkIn, LocalDate checkOut) {
        if (checkIn.isAfter(checkOut)) {
            throw new IllegalPersonalDetailsException(checkOut.toString());
        }
        return checkOut;
    }
}