package hotel.front;

import hotel.exception.HotelServiceException;
import hotel.exception.NoRoomsFoundException;
import hotel.model.Guest;
import hotel.model.PersonalDetails;
import hotel.model.Room;
import hotel.service.UserService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Controller {

    private UserService userService;
    private Messages messages;
    private Input input;
    private boolean running = true;

    public Controller(UserService userService) {
        this.userService = userService;
        input = new Input();
        messages = new Messages();
    }

    public void start() {
        do {
            checkAndInteract();
        } while (running);
    }

    private void checkAndInteract() {
        try {
            interact();
        } catch (HotelServiceException e) {
            System.err.println(e.getMessage());
        }
    }

    private void interact() {
        printMenu();
        int chosenOption = input.mainOption();
        executeOption(chosenOption);
    }

    private void printMenu() {
        System.out.println(messages.getOptions());
    }

    private void executeOption(int input) {
        if (input == 1) printRooms();
        else if (input == 2) printAvailableRooms();
        else if (input == 3) printRentedRooms();
        else if (input == 4) bookRoom();
        else if (input == 5) releaseRoom();
        else if (input == 6) cleanRoom();
        else end();
    }

    private void printRooms() {
        messages.printListRoom(userService.getAllRoom());
    }

    private void printAvailableRooms() {
        messages.printListRoom(userService.getAvailableAndCleanedRoom());
    }

    private void printRentedRooms() {
        messages.printListRoom(userService.getRentedRoom());
    }

    private void end() {
        System.out.println(messages.bye());
        input.close();
        running = false;
    }

    private void bookRoom() {
        checkIfThereAreRooms(userService.getAvailableAndCleanedRoom());
        printAvailableRooms();
        int number = getInputRoom();
        List<LocalDate> dates = datesOfStay();
        List<Guest> guestList = getGuestsPersonalDetails(userService.getRoom(number));
        userService.bookRoom(number, dates.get(0), dates.get(1), guestList);
        System.out.println(messages.bookedRoom() + --number);
    }

    private void releaseRoom() {
        checkIfThereAreRooms(userService.getRentedRoom());
        printRentedRooms();
        List<Room> rentedRoom = userService.getRentedRoom();
        int inputs = input.getValidRoom(rentedRoom);
        int number = findRoomNumber(inputs, rentedRoom);
        userService.releaseRoom(number);
        System.out.println(messages.releasedRoom() + inputs);
        System.out.println(messages.seeYou());
    }

    private void cleanRoom() {
        checkIfThereAreRooms(userService.getNotCleanedRoom());
        List<Room> notCleanedRooms = userService.getNotCleanedRoom();
        messages.printListRoom(notCleanedRooms);
        int chosen = input.getValidRoom(notCleanedRooms);
        int number = findRoomNumber(chosen, userService.getNotCleanedRoom());
        userService.cleanRoom(number);
        System.out.println(messages.successfullyCleaned() + chosen);
    }

    private void checkIfThereAreRooms(List<Room> roomList) {
        if (roomList.isEmpty()) {
            throw new NoRoomsFoundException();
        }
    }

    private int getInputRoom() {
        System.out.println(messages.whichBook());
        List<Room> availableRooms = userService.getAvailableAndCleanedRoom();
        int chosenRoom = input.getValidRoom(availableRooms);
        return findRoomNumber(chosenRoom, userService.getAvailableAndCleanedRoom());
    }

    private List<LocalDate> datesOfStay() {
        List<LocalDate> list = new ArrayList<>();
        LocalDate checkIn = readCheckIn();
        list.add(checkIn);
        list.add(readCheckOut(checkIn));
        return list;
    }

    private LocalDate readCheckIn() {
        System.out.println(messages.whenToCheckIn());
        return validateAndParseDate();
    }

    private LocalDate readCheckOut(LocalDate signIn) {
        System.out.println(messages.whenToCheckOut());
        LocalDate date = validateAndParseDate();
        return input.wheterCheckInDateOlder(signIn, date);
    }

    private LocalDate validateAndParseDate() {
        LocalDate localDate;
        localDate = input.getValidAndParseDate();
        return localDate;
    }

    private List<Guest> getGuestsPersonalDetails(Room room) {
        int guests = room.getHowManyPeople();
        return readPersonalDetails(guests);
    }

    private List<Guest> readPersonalDetails(int guests) {
        List<Guest> guestList = new ArrayList<>();
        for (int i = 0; i < guests; i++) {
            String name = readAndValidData(i, PersonalDetails.NAME);
            String surname = readAndValidData(i, PersonalDetails.SURNAME);
            messages.printAddingIndicator(i, String.valueOf(PersonalDetails.DATE_OF_BIRTH).toLowerCase());
            LocalDate date = input.getValidDate();
            guestList.add(userService.createGuest(name, surname, date));
        }
        return guestList;
    }

    private String readAndValidData(int index, PersonalDetails details) {
        String nameOrSurname;
        if (details.equals(PersonalDetails.NAME)) {
            messages.printAddingIndicator(index, String.valueOf(PersonalDetails.NAME).toLowerCase());
            nameOrSurname = input.getValidName();
        } else {
            messages.printAddingIndicator(index, String.valueOf(PersonalDetails.SURNAME).toLowerCase());
            nameOrSurname = input.getValidSurname();
        }
        return nameOrSurname;
    }

    private int findRoomNumber(int input, List<Room> rooms) {
        Room room = UserService.findRoom(convertInputToIndex(input), rooms);
        return room.getNumber();
    }

    private int convertInputToIndex(int input) {
        return --input;
    }

}
