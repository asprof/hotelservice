package hotel.front;

import hotel.model.Guest;
import hotel.model.Room;

import java.time.LocalDate;
import java.util.List;

public class Messages {

    protected String getOptions() {
        return createOptions();
    }

    private String createOptions() {
        String nextLine = "\n";
        StringBuilder options = new StringBuilder();
        options.append("Welcome to Hotel Service Program").append(nextLine)
                .append("How can I help you ?").append(nextLine)
                .append("0. Exit a program").append(nextLine)
                .append("1. Display rooms and rooms status").append(nextLine)
                .append("2. Display available a rooms").append(nextLine)
                .append("3. Display rented rooms").append(nextLine)
                .append("4. To book a room").append(nextLine)
                .append("5. To releas a room").append(nextLine)
                .append("6. Clean a room").append(nextLine);
        return options.toString();
    }

    protected void printListRoom(List<Room> list) {
        for (int i = 0; i < list.size(); i++) {
            Room room = list.get(i);
            System.out.printf("%3d. %s \n", i + 1, basicTextFormat(room.getNumber(),
                    room.getHowManyPeople(),
                    room.getBathroom(),
                    room.getAvailable(),
                    room.getCleaned(),
                    room.getCheckInDate(),
                    room.getCheckOutDate(),
                    room.getListGuests())
            );

        }
    }

    private String basicTextFormat(
            int number, int howManyPeople,
            boolean bathroom, boolean available, boolean cleaned,
            LocalDate checkIn, LocalDate checkOut, List<Guest> guestList) {
        StringBuilder format = new StringBuilder();
        format.append("Room{number=").append(number)
                .append(", number of people=").append(howManyPeople)
                .append(", bathroom=").append(bathroom)
                .append(", available=").append(available)
                .append(", cleaned=").append(cleaned);
        if (checkIn != LocalDate.MIN && checkOut != LocalDate.MAX && !guestList.isEmpty()) {
            format.append(", check in=").append(checkIn)
                    .append(", check out=").append(checkOut)
                    .append(", ").append(guestList);
        }
        format.append("}");
        return format.toString();
    }

    protected void printAddingIndicator(int index, String personalDetails) {
        StringBuilder message = new StringBuilder();
        message.append("Write ")
                .append(index + 1)
                .append(". person's ")
                .append(personalDetails);
        if (personalDetails.compareTo("date of birth") == 0) {
            message.append(" (year-month-day): ");
        } else {
            message.append(": ");
        }
        System.out.println(message.toString());
    }

    protected String bye() {
        return "Good bye and have a nice day! :)";
    }

    protected String whichBook() {
        return "Which room do you want to book ?";
    }

    private String when() {
        return "When do you want to";
    }

    protected String whenToCheckIn() {
        return when() + " check in ?";
    }

    protected String whenToCheckOut() {
        return when() + " check out ?";
    }

    protected String bookedRoom() {
        return "Congratulation. You have successfully booked the room ";
    }

    public String releasedRoom() {
        return "You have successfully released the room ";
    }

    public String seeYou() {
        return "We are looking forward to meeting you again";
    }

    public String successfullyCleaned() {
        return "You have successfully cleaned the room ";
    }
}
